from _curses import flash

from flask import render_template, request
from flask import Flask
from models.Endereco import Endereco
from models.Pessoa import Pessoa
from daos.EnderecoDAO import EnderecoDAO
from daos.PessoaDAO import PessoaDAO
from daos.PacienteDAO import PacienteDAO
from .modelos.Paciente import Paciente
from .modelos.Medico impor Medico


app = Flask(__name__)
app.config.from_pyfile('config.py')


@app.route("/", methods=["GET","POST"])
def login():
    if request.form:
        print(request.form)
    return render_template('login.html')

@app.route('/home/')
def home():
    return render_template('index.html')

@app.route('/pessoa/', methods=["GET","POST"])
def pessoa():
    if request.form:
        rua = request.form['nm_rua']
        bairro = request.form['nm_bairro']
        cidade = request.form['nm_cidade']
        uf = request.form['nm_uf']
        cep = request.form['cd_cep']
        pessoa_endereco = Endereco(nm_rua=rua, nm_bairro=bairro, nm_cidade=cidade, nm_uf=uf, cd_cep=cep)
        pessoa_endereco = EnderecoDAO(pessoa_endereco).salva()
        print(pessoa_endereco.cd_endereco)
        nome = request.form['nm_pessoa']
        nascimento = request.form['dt_nascimento']
        rg = request.form['nr_rg']
        cpf = request.form['nr_cpf']
        genero = request.form['fg_genero']
        telefone = request.form['nr_telefone']
        pessoa_cadastro = Pessoa(nm_pessoa=nome, dt_nascimento=nascimento, nr_rg=rg, nr_cpf=cpf, fg_genero=genero, nr_telefone=telefone,
                 endereco_cd=pessoa_endereco.cd_endereco)
        pessoa_cadastro = PessoaDAO(pessoa_cadastro).salva()
        #flash("Cadastro realizado com sucesso")
    return render_template('pessoa.html')

@app.route('/paciente/', methods=['POST','GET'])
def paciente():
    # if request.form == 'POST':
    #     pessoa_paciente = Pessoa()
    #     pessoa_paciente = PessoaDAO(Pessoa()).busca_pessoa(nr_cpf= resquest.form['busca_paciente'])
    #     print(pessoa_paciente)
    #     paciente_cadastro = Paciente(pessoa,
    #                                  cd_paciente=pessoa_paciente.cd_pessoa,
    #                                  de_diagnostico=request.form['diagnostico'],
    #                                  cd_medico_responsavel=request.form['medico'],
    #                                  dt_entrada_espera=request.form['data_entrada'],
    #                                  hr_entrada_espera=request.form['hora_entrada'],
    #                                  fg_atendido=request.form['atendido'],
    #                                  cd_gravidade=request.form['gravidade'])
    #     paciente_cadastro = PacienteDAO(paciente_cadastro).salva()

    return render_template('paciente.html')

@app.route('/medico/', methods=['POST', 'GET'])
def medico():
    if request.form:
        print(request.form)
        medico_cadastrar = Medico()
    return render_template('medico.html')

################
## Relatórios##
###############

@app.route('/relatorios/pacientes/')
def pacientes():
    return render_template('pacientes.html')

@app.route('/relatorios/medicos/')
def medicos():
    return render_template('relatorios/medicos.html')

@app.route('/relatorios/gravidades/')
def paciente_gravidade():
    return render_template('gravidade.html')

@app.route('/relatorios/acompanhamentos/')
def acompanhamentos():
    return render_template('acompanhamentos.html')

@app.route('/relatorios/estadias/')
def estadias():
    return render_template('estadias.html')

@app.route('/relatorios/logs/')
def logs():
    return render_template('logs_sistema.html')


if __name__ == '__main__':
    from flask_sqlalchemy import get_debug_queries
    app.run(debug=True)