from daos.Dao import Dao
from models.Acompanhamento import Acompanhamento


SQL_INSERT_ACOMPANHAMENTO = 'INSERT INTO "Acompanhamento" (dt_visita, nr_hora, de_estado, vl_pressao, vl_temparatura, ' \
                            'cd_medico_visita, cd_paciente) VALUES (%s, %s, %s, %s, %s, %s, %s) ' \
                            'RETURNING cd_acompanhamento;'

SQL_UPDATE_ACOMPANHAMENTO = 'UPDATE "Acompanhamento" SET dt_visita=%s, nr_hora=%s, de_estado=%s, vl_pressao=%s, ' \
                            'vl_temperatura=%s, cd_medico_visita=%s, cd_paciente=%s WHERE cd_acompanhamento=%s;'

SQL_SELECT_ACOMPANHAMENTOS = 'SELECT * FROM "Acompanhamento";'


class AcompanhamentoDAO(Dao):
    def __init__(self, acompanhamento=None):
        Dao.__init__(self)
        self.__acompanhamento = acompanhamento

    @property
    def acompanhamento(self):
        return self.__acompanhamento

    @acompanhamento.setter
    def acompanhamento(self, acompanhamento):
        self.__acompanhamento = acompanhamento

    def salva(self):
        cursor = self.db.cursor()

        if self.acompanhamento.cd_acompanhamento:
            cursor.execute(SQL_UPDATE_ACOMPANHAMENTO, (self.acompanhamento.dt_visita,
                                                       self.acompanhamento.nr_hora,
                                                       self.acompanhamento.de_estado,
                                                       self.acompanhamento.vl_pressao,
                                                       self.acompanhamento.vl_temperatura,
                                                       self.acompanhamento.cd_medico_visita,
                                                       self.acompanhamento.cd_pessoa,
                                                       self.acompanhamento.cd_acompanhamento))
        else:
            cursor.execute(SQL_INSERT_ACOMPANHAMENTO, (self.acompanhamento.dt_visita,
                                                       self.acompanhamento.nr_hora,
                                                       self.acompanhamento.de_estado,
                                                       self.acompanhamento.vl_pressao,
                                                       self.acompanhamento.vl_temperatura,
                                                       self.acompanhamento.cd_medico_visita,
                                                       self.acompanhamento.cd_pessoa))

            self.acompanhamento.cd_acompanhamento = cursor.fetchone()[0]

        self.db.commit()
        return self.acompanhamento

    def lista_todos(self):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_ACOMPANHAMENTOS)
        acompanhamentos = self.traduz_acompanhamento(cursor.fetchall())
        return acompanhamentos

    @staticmethod
    def traduz_acompanhamento(acompanhamentos):
        def cria_acompanhamento(acompanhamento):
            return Acompanhamento(cd_acompanhemento=acompanhamento[0], dt_visita=acompanhamento[1],
                                  nr_hora=acompanhamento[2], de_estado=acompanhamento[3], vl_pressao=acompanhamento[4],
                                  vl_temperatura=acompanhamento[5], cd_medico_visita=acompanhamento[6],
                                  cd_paciente=acompanhamento[7])

        return list(map(cria_acompanhamento, acompanhamentos))
