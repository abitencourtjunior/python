from daos import db


class Dao(object):
    def __init__(self):
        self.__db = db

    @property
    def db(self):
        return self.__db
