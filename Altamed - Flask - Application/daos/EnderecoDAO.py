from daos.Dao import Dao
from models.Endereco import Endereco


SQL_INSERT_ENDERECO = 'INSERT INTO "Endereco" (nm_rua, nm_bairro, nm_cidade, nm_uf, cd_cep) VALUES (%s, %s, %s, %s, %s)' \
                      'RETURNING cd_endereco;'

SQL_SELECT_ENDERECO = 'SELECT cd_endereco, nm_rua, nm_bairro, nm_cidade, nm_uf, cd_cep from "Endereco" order by nm_uf, ' \
                      'nm_cidade, nm_bairro, cd_cep, nm_rua;'

SQL_SELECT_ENDERECO_CEP_RUA = 'SELECT cd_endereco, nm_rua, nm_bairro, nm_cidade, nm_uf, cd_cep from "Endereco" ' \
                              'where nm_rua=%s and cd_cep=%s order by nm_uf, nm_cidade, nm_bairro, cd_cep, nm_rua'


class EnderecoDAO(Dao):
    def __init__(self, endereco=None):
        Dao.__init__(self)
        self.__endereco = endereco

    @property
    def endereco(self):
        return self.__endereco

    @endereco.setter
    def endereco(self, endereco):
        self.__endereco = endereco

    def salva(self):
        cursor = self.db.cursor()

        if self.endereco.cd_endereco is None:

            cursor.execute(SQL_INSERT_ENDERECO, (self.endereco.nm_rua, self.endereco.nm_bairro, self.endereco.nm_cidade,
                                                 self.endereco.nm_uf, self.endereco.cd_cep))
            self.endereco.cd_endereco = cursor.fetchone()[0]

        self.db.commit()

        return self.endereco

    def lista_todos(self):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_ENDERECO)
        enderecos = self.traduz_endereco(cursor.fetchall())
        return enderecos

    def busca_endereco(self, nm_rua, cd_cep):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_ENDERECO_CEP_RUA, (nm_rua, cd_cep))
        endereco = cursor.fetchone()
        self.__endereco = Endereco(cd_endereco=endereco[0], nm_rua=endereco[1], nm_bairro=endereco[2],
                                   nm_cidade=endereco[3], nm_uf=endereco[4], cd_cep=endereco[5])
        return self.endereco

    @staticmethod
    def traduz_endereco(enderecos):
        def cria_endereco(endereco):
            return Endereco(cd_endereco=endereco[0], nm_rua=endereco[1], nm_bairro=endereco[2], nm_cidade=endereco[3],
                            nm_uf=endereco[4], cd_cep=endereco[5])

        return list(map(cria_endereco, enderecos))
