from daos.Dao import Dao
from models.Especialidade import Especialidade


SQL_SELECT_ESPECIALIDADES = 'SELECT cd_especialidade, nm_especialidade from "Especialidade" order by nm_especialidade;'
SQL_SELECT_ESPECIALIDADE = 'SELECT cd_especialidade, nm_especialidade from "Especialidade" where cd_especialidade=%s;'


class EspecialidadeDAO(Dao):
    def __init__(self, especialidade=None):
        Dao.__init__(self)
        self.__especialidade = especialidade

    @property
    def especialidade(self):
        return self.__especialidade

    @especialidade.setter
    def especialidade(self, especialidade):
        self.__especialidade = especialidade

    def lista_todas(self):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_ESPECIALIDADES)
        especialidades = self.traduz_especialidade(cursor.fetchall())
        return especialidades

    def busca_especialidade(self, cd_especialidade):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_ESPECIALIDADE, (cd_especialidade,))
        especialidade = cursor.execute.fetchone()
        self.especialidade = Especialidade(cd_especialidade=especialidade[0], nm_especialidade=especialidade[1])
        return self.especialidade

    @staticmethod
    def traduz_especialidade(especialidades):
        def cria_especialidade(especialidade):
            return Especialidade(cd_especialidade=especialidade[0], nm_especialidade=especialidade[1])

        return list(map(cria_especialidade, especialidades))