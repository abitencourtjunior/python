from daos.Dao import Dao
from models.Estadia import Estadia


SQL_INSERT_ESTADIA = 'INSERT INTO "Estadia" (cd_quarto, cd_paciente, fg_infeccao, dt_infeccao, de_sintomas) VALUES ' \
                     '(%s, %s, %s, %s, %s);'

SQL_UPDATE_ESTADIA = 'UPDATE "Estadia" SET fg_infeccao= %s, dt_infeccao= %s, de_sintomas= %s ' \
                     'WHERE cd_quarto=%s AND cd_paciente=%s;'

SQL_SELECT_ESTADIA = 'SELECT cd_quarto, cd_paciente, fg_infeccao, dt_infeccao, de_sintomas FROM "Estadia" ' \
                     'WHERE cd_quarto=%s AND cd_paciente=%s;'

SQL_SELECT_ESTADIAS = 'SELECT cd_quarto, cd_paciente, fg_infeccao, dt_infeccao, de_sintomas FROM "Estadia";'


class EstadiaDAO(Dao):
    def __init__(self, estadia=None):
        Dao.__init__(self)
        self.__estadia = estadia

    @property
    def estadia(self):
        return self.__estadia

    @estadia.setter
    def estadia(self, estadia):
        self.__estadia = estadia

    def salva(self):
        cursor = self.db.cursor()

        if self.estadia == self.busca_estadia(self.estadia.cd_quarto, self.estadia.cd_paciente):

            cursor.execute(SQL_UPDATE_ESTADIA, (self.estadia.fg_infeccao, self.estadia.dt_infeccao,
                                                self.estadia.de_sintomas, self.estadia.cd_quarto,
                                                self.estadia.cd_paciente))

        else:
            cursor.execute(SQL_INSERT_ESTADIA, (self.estadia.cd_quarto, self.estadia.cd_paciente,
                                                self.estadia.fg_infeccao, self.estadia.dt_infeccao,
                                                self.estadia.de_sintomas))

        self.db.commit()
        return self.estadia

    def lista_todas(self):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_ESTADIAS)
        estadias = self.traduz_estadia(cursor.fetchall())
        return estadias

    def busca_estadia(self, cd_quarto, cd_paciente):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_ESTADIA, (cd_quarto, cd_paciente))
        estadia = cursor.fetchone()
        return Estadia(cd_quarto=estadia[0], cd_paciente=estadia[1], fg_infeccao=estadia[2], dt_infeccao=estadia[3],
                       de_sintomas=estadia[4])

    @staticmethod
    def traduz_estadia(estadias):
        def cria_estadia(estadia):
            return Estadia(cd_quarto=estadia[0], cd_paciente=estadia[1], fg_infeccao=estadia[2], dt_infeccao=estadia[3],
                           de_sintomas=estadia[4])

        return list(map(cria_estadia, estadias))
