from daos.Dao import Dao
from models.Formacao import Formacao


SQL_INSERT_FORMACAO = 'INSERT INTO "Formacao" (cd_medico, cd_especialidade, fg_ativo) values (%s, %s, %s);'

SQL_UPDATE_FORMACAO = 'UPDATE "Formacao" SET fg_ativo= %s WHERE cd_medico=%s AND cd_especialidade=%s;'

SQL_SELECT_FORMACAO = 'SELECT cd_medico, cd_especialidade, fg_ativo from "Especialidade" ' \
                      'WHERE cd_medico=%s and cd_especialidade=%s;'

SQL_SELECT_FORMACOES = 'SELECT cd_medico, cd_especialidade, fg_ativo from "Especialidade";'


class FormacaoDAO(Dao):
    def __init__(self, formacao=None):
        Dao.__init__(self)
        self.__formacao = formacao

    @property
    def formacao(self):
        return self.__formacao

    @formacao.setter
    def formacao(self, formacao):
        self.__formacao = formacao

    def salva(self):
        cursor = self.db.cursor()

        if self.formacao == self.busca_formacao(self.formacao.cd_medico, self.formacao.cd_especialidade):
            cursor.execute(SQL_UPDATE_FORMACAO, (self.formacao.fg_ativo, self.formacao.cd_medico,
                                                 self.formacao.cd_especialidade))

        else:
            cursor.execute(SQL_INSERT_FORMACAO, (self.formacao.cd_medico, self.formacao.cd_especialidade,
                                                 self.formacao.fg_ativo))

        self.db.commit()
        return self.formacao

    def lista_todas(self):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_FORMACOES)
        formacoes = self.traduz_formacao(cursor.fetchall())
        return formacoes

    def busca_formacao(self, cd_medico, cd_especialidade):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_FORMACAO, (cd_medico, cd_especialidade))
        formacao = cursor.fetchone()
        return Formacao(cd_medico=formacao[0], cd_especialidade=formacao[1], fg_ativo=formacao[2])

    @staticmethod
    def traduz_formacao(formacoes):
        def cria_formacao(formacao):
            return Formacao(cd_medico=formacao[0], cd_especialidade=formacao[1], fg_ativo=formacao[2])
        return list(map(cria_formacao, formacoes))
