from daos.Dao import Dao
from models.Log import Log

SQL_INSERT_LOG = 'INSERT INTO "Log" (de_log, dt_log, hr_log) VALUES (%s, %s, %s) RETURNING cd_log;'

SQL_SELECT_LOGS = 'SELECT cd_log, de_log, dt_log, hr_log FROM "Log" ORDER BY cd_log DESC;'

SQL_SELECT_LOG = 'SELECT cd_log, de_log, dt_log, hr_log FROM "Log" WHERE dt_log = %s;'


class LogDAO(Dao):
    def __init__(self, log=None):
        Dao.__init__(self)
        self.__log = log

    @property
    def log(self):
        return self.__log

    @log.setter
    def log(self, log):
        self.__log = log

    def salva(self):
        cursor = self.db.cursor()

        if self.log.cd_log is None:
            cursor.execute(SQL_INSERT_LOG, (self.log.dt_log, self.log.dt_log, self.log.hr_log))
            self.log.cd_log = cursor.fetchone()[0]

        self.db.commit()
        return self.log

    def lista_todos(self):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_LOGS)
        logs = self.traduz_log(cursor.fetchall())
        return logs

    def busca_log(self, dt_log):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_LOG, (dt_log,))
        log = cursor.fetchone()
        return Log(cd_log=log[0], de_log=log[1], dt_log=log[2], hr_log=log[3])

    @staticmethod
    def traduz_log(logs):
        def cria_log(log):
            return Log(cd_log=log[0], de_log=log[1], dt_log=log[2], hr_log=log[3])

        return list(map(cria_log, logs))
