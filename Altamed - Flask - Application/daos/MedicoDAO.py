from daos.Dao import Dao
from models.Medico import Medico
from models.Pessoa import Pessoa
from models.Especialidade import Especialidade


SQL_INSERT_MEDICO = 'INSERT INTO "Medico" (cd_pessoa, nr_crm, fg_ativo, vl_salario) VALUES (%s, %s, %s, %s) ' \
                    'RETURNING cd_medico;'

SQL_UPDATE_MEDICO = 'UPDATE "Medico" SET fg_ativo= %s, vl_salario= %s WHERE cd_medico=%s OR nr_crm=%s;'

SQL_SELECT_MEDICO_ID = 'SELECT * FROM vw_medico_completo WHERE cd_medico=%s;'

SQL_SELECT_MEDICO_CRM = 'SELECT * FROM vw_medico_completo WHERE nr_crm=%s;'

SQL_SELECT_MEDICOS = 'SELECT * FROM vw_medico_completo;'


class MedicoDAO(Dao):
    def __init__(self, medico=None):
        Dao.__init__(self)
        self.__medico = medico

    @property
    def medico(self):
        return self.__medico

    @medico.setter
    def medico(self, medico):
        self.__medico = medico

    def salva(self):
        cursor = self.db.cursor()

        if self.medico.cd_medico:
            cursor.execute(SQL_UPDATE_MEDICO, (self.medico.fg_ativo, self.medico.vl_salario, self.medico.cd_medico))

        else:
            cursor.execute(SQL_INSERT_MEDICO, (self.medico.cd_pessoa, self.medico.nr_crm, self.medico.fg_ativo,
                                               self.medico.vl_salario))
            self.medico.cd_medico = cursor.fetchone()[0]

        self.db.commit()
        return self.medico

    def busca_medico(self, cd_medico=None, nr_crm=None):
        cursor = self.db.cursor()
        medico = None
        if cd_medico:
            cursor.execute(SQL_SELECT_MEDICO_ID, (cd_medico,))
            medico = cursor.fetchone()

        elif nr_crm:
            cursor.execute(SQL_SELECT_MEDICO_CRM, (nr_crm,))
            medico = cursor.fetchone()

        return Medico(Pessoa(cd_pessoa=medico[6], nm_pessoa=medico[7], dt_nascimento=medico[8], nr_rg=medico[9],
                             nr_cpf=medico[10], fg_genero=medico[11], nr_telefone=medico[12],
                             endereco_cd=medico[13]),
                      cd_medico=medico[0], nr_crm=medico[1], fg_ativo=medico[2],
                      especialidade=Especialidade(cd_especialidade=medico[4], nm_especialidade=medico[5]))

    def lista_todos(self):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_MEDICOS)
        medicos = self.traduz_medicos(cursor.fetchall())
        return medicos

    @staticmethod
    def traduz_medicos(medicos):
        def cria_medico(medico):
            return Medico(Pessoa(cd_pessoa=medico[6], nm_pessoa=medico[7], dt_nascimento=medico[8], nr_rg=medico[9],
                          nr_cpf=medico[10], fg_genero=medico[11], nr_telefone=medico[12], endereco_cd=medico[13]),
                          cd_medico=medico[0], nr_crm=medico[1], fg_ativo=medico[2],
                          especialidade=Especialidade(cd_especialidade=medico[4], nm_especialidade=medico[5]) )

        return list(map(cria_medico, medicos))
