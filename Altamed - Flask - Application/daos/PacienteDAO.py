from daos.Dao import Dao
from models.Paciente import Paciente
from models.Pessoa import Pessoa
from models.Medico import Medico


SQL_INSERT_PACIENTE = 'INSERT INTO "Paciente" (de_diagnostico, pessoa_cd, cd_medico_responsavel, dt_entrada_espera, ' \
                      'hr_entrada_espera, fg_atendido, cd_gravidade) VALUES (%s, %s, %s, %s, %s, %s, %s) ' \
                      'RETURNING cd_paciente;'

SQL_UPDATE_PACIENTE = 'UPDATE "Paciente" SET de_diagnostico= %s, cd_medico_responsavel=%s, dt_entrada_espera=%s, ' \
                      'hr_entrada_espera= %s, fg_atendido= %s, cd_gravidade= %s WHERE cd_paciente=%s;'

SQL_SELECT_PACIENTES_ID = 'SELECT * FROM vw_pacientes_ordenados WHERE cd_paciente= %s;'

SQL_SELECT_PACIENTES = 'SELECT * FROM vw_pacientes_ordenados;'


class PacienteDAO(Dao):
    def __init__(self, paciente=None):
        Dao.__init__(self)
        self.__paciente = paciente

    @property
    def paciente(self):
        return self.__paciente

    @paciente.setter
    def paciente(self, paciente):
        self.__paciente = paciente

    def salva(self):
        cursor = self.db.cursor()

        if self.paciente.cd_paciente:
            cursor.execute(SQL_UPDATE_PACIENTE, (self.paciente.de_diagnostico, self.paciente.cd_medico_responsavel,
                                                 self.paciente.dt_entrada_espera, self.paciente.hr_entrada_espera,
                                                 self.paciente.fg_atendido, self.paciente.cd_gravidade,
                                                 self.paciente.cd_paciente))

        else:
            cursor.execute(SQL_INSERT_PACIENTE, (self.paciente.de_diagnostico, self.paciente.cd_medico_responsavel,
                                                 self.paciente.dt_entrada_espera, self.paciente.hr_entrada_espera,
                                                 self.paciente.fg_atendido, self.paciente.cd_gravidade))
            self.paciente.cd_paciente = cursor.fetchone()[0]

        self.db.connection.commit()
        return self.paciente

    def busca_paciente(self, cd_paciente):
        cursor = self.db.cursor()
        if cd_paciente:
            cursor.execute(SQL_SELECT_PACIENTES_ID, (cd_paciente,))
            paciente = cursor.fetchone()

            return Paciente(Pessoa(nm_pessoa=paciente[1]), cd_paciente=paciente[0], de_diagnostico=paciente[2],
                        dt_entrada_espera=paciente[4], hr_entrada_espera=paciente[5], fg_atendido=paciente[6],
                        cd_gravidade=paciente[7], cd_medico_responsavel=Medico(nr_crm=paciente[3], pessoa=Pessoa()))

    def lista_todos(self):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_PACIENTES)
        pacientes = self.traduz_pacientes(cursor.fetchall())
        return pacientes

    @staticmethod
    def traduz_pacientes(pacientes):
        def cria_paciente(paciente):
            return Paciente(Pessoa(nm_pessoa=paciente[1]), cd_paciente=paciente[0], de_diagnostico=paciente[2],
                            dt_entrada_espera=paciente[4], hr_entrada_espera=paciente[5], fg_atendido=paciente[6],
                            cd_gravidade=paciente[7], cd_medico_responsavel=Medico(nr_crm=paciente[3], pessoa=Pessoa()))

        return list(map(cria_paciente, pacientes))
