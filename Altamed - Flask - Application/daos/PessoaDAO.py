from daos.Dao import Dao
from models.Pessoa import Pessoa


SQL_INSERT_PESSOA = 'INSERT INTO "Pessoa" (nm_pessoa, dt_nascimento, nr_rg, nr_cpf, fg_genero, nr_telefone, endereco_cd)' \
                    ' VALUES (%s, %s, %s, %s, %s, %s, %s) RETURNING cd_pessoa;'

SQL_UPDATE_PESSOA = 'UPDATE "Pessoa" SET nm_pessoa= %s, dt_nascimento= %s, nr_rg= %s, nr_cpf = %s, fg_genero= %s, ' \
                    'nr_telefone= %s, endereco_cd= %s WHERE cd_pessoa= %s;'

SQL_SELECT_PESSOA_ID = 'SELECT cd_pessoa, nm_pessoa, dt_nascimento, nr_rg, nr_cpf, fg_genero, nr_telefone, endereco_cd ' \
                       'FROM "Pessoa" WHERE cd_pessoa= %s;'

SQL_SELECT_PESSOA_CPF = 'SELECT cd_pessoa, nm_pessoa, dt_nascimento, nr_rg, nr_cpf, fg_genero, nr_telefone, endereco_cd ' \
                       'FROM "Pessoa" WHERE nr_cpf= %s;'

SQL_SELECT_PESSOAS = 'SELECT cd_pessoa, nm_pessoa, dt_nascimento, nr_rg, nr_cpf, fg_genero, nr_telefone, endereco_cd ' \
                       'FROM "Pessoa";'


class PessoaDAO(Dao):
    def __init__(self, pessoa=None):
        Dao.__init__(self)
        self.__pessoa = pessoa

    @property
    def pessoa(self):
        return self.__pessoa

    @pessoa.setter
    def pessoa(self, pessoa):
        self.__pessoa = pessoa

    def salva(self):
        cursor = self.db.cursor()

        if self.pessoa.cd_pessoa:
            cursor.execute(SQL_UPDATE_PESSOA, (self.pessoa.nm_pessoa, self.pessoa.dt_nascimento, self.pessoa.nr_rg,
                                               self.pessoa.nr_cpf, self.pessoa.fg_genero, self.pessoa.nr_telefone,
                                               self.pessoa.endereco_cd, self.pessoa.cd_pessoa))

        else:
            cursor.execute(SQL_INSERT_PESSOA, (self.pessoa.nm_pessoa, self.pessoa.dt_nascimento, self.pessoa.nr_rg,
                                               self.pessoa.nr_cpf, self.pessoa.fg_genero, self.pessoa.nr_telefone,
                                               self.pessoa.endereco_cd))
            self.pessoa.cd_pessoa = cursor.fetchone()[0]

        self.db.commit()
        return self.pessoa

    def busca_pessoa(self, cd_pessoa=None, nr_cpf=None):
        print('passou aqui')
        cursor = self.db.cursor()

        if cd_pessoa:
            cursor.execute(SQL_SELECT_PESSOA_ID, (cd_pessoa,))
            pessoa = cursor.fetchone()
            return Pessoa(cd_pessoa=pessoa[0], nm_pessoa=pessoa[1], dt_nascimento=pessoa[2], nr_rg=pessoa[3],
                          nr_cpf=pessoa[4], fg_genero=pessoa[5], nr_telefone=pessoa[6], endereco_cd=pessoa[7])

        elif nr_cpf:
            cursor.execute(SQL_SELECT_PESSOA_CPF, (nr_cpf,))
            pessoa = cursor.fetchone()
            return Pessoa(cd_pessoa=pessoa[0], nm_pessoa=pessoa[1], dt_nascimento=pessoa[2], nr_rg=pessoa[3],
                          nr_cpf=pessoa[4], fg_genero=pessoa[5], nr_telefone=pessoa[6], endereco_cd=pessoa[7])

    def lista_todas(self):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_PESSOAS)
        pessoas = self.traduz_pessoas(cursor.fetchall())
        return pessoas

    @staticmethod
    def traduz_pessoas(pessoas):
        def cria_pessoa(pessoa):
            return Pessoa(cd_pessoa=pessoa[0], nm_pessoa=pessoa[1], dt_nascimento=pessoa[2], nr_rg=pessoa[3],
                          nr_cpf=pessoa[4], fg_genero=pessoa[5], nr_telefone=pessoa[6], endereco_cd=pessoa[7])

        return list(map(cria_pessoa, pessoas))
