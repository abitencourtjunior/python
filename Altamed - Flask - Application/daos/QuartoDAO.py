from daos.Dao import Dao
from models.Quarto import Quarto


SQL_INSERT_QUARTO = 'INSERT INTO "Quarto" (nr_andar, nr_capacidade, nr_quarto) VALUES (%s, %s, %s) RETURNING cd_quarto;'

SQL_UPDATE_QUARTO = 'UPDATE "Quarto" SET nr_andar= %s, nr_capacidade= %s, nr_quarto= %s WHERE cd_quarto=%s;'

SQL_SELECT_QUARTOS = 'SELECT cd_quarto, nr_andar, nr_capacidade, nr_quarto from "Quarto" ORDER BY nr_andar, nr_quarto;'

SQL_SELECT_QUARTO = 'SELECT cd_quarto, nr_andar, nr_capacidade, nr_quarto from "Quarto" WHERE cd_quarto = %s;'


class QuartoDAO(Dao):
    def __init__(self, quarto=None):
        Dao.__init__(self)
        self.__quarto = quarto

    @property
    def quarto(self):
        return self.__quarto

    @quarto.setter
    def quarto(self, quarto):
        self.__quarto = quarto

    def salva(self):
        cursor = self.db.cursor()

        if self.quarto.cd_quarto:
            cursor.execute(SQL_UPDATE_QUARTO, (self.quarto.nr_andar, self.quarto.nr_capacidade, self.quarto.nr_quarto,
                                               self.quarto.cd_quarto))
        else:
            cursor.execute(SQL_INSERT_QUARTO, (self.quarto.nr_andar, self.quarto.nr_capacidade, self.quarto.nr_quarto))

            self.quarto.cd_quarto = cursor.fetchone()[0]

        self.db.commit()
        return self.quarto

    def lista_todos(self):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_QUARTOS)
        quartos = self.traduz_quartos(cursor.fetchall())
        return quartos

    def busca_quarto(self, cd_quarto):
        cursor = self.db.cursor()
        cursor.execute(SQL_SELECT_QUARTO, (cd_quarto,))
        quarto = cursor.fetchone()
        return Quarto(cd_quarto=quarto[0], nr_andar=quarto[1], nr_capacidade=quarto[2], nr_quarto=quarto[3])

    @staticmethod
    def traduz_quartos(quartos):
        def cria_quarto(quarto):
            return Quarto(cd_quarto=quarto[0], nr_andar=quarto[1], nr_capacidade=quarto[2], nr_quarto=quarto[3])

        return list(map(cria_quarto, quartos))
