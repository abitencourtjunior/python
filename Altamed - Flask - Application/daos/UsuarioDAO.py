from daos.Dao import Dao
from models.Usuario import Usuario


SQL_INSERT_USUARIO = 'INSERT INTO "Usuario" (cd_email, cd_senha, cd_medico) VALUES (%s, %s, %s) RETURNING cd_usuario;'

SQL_UPDATE_USUARIO = 'UPDATE "Usuario" SET cd_email= %s, cd_senha= %s, cd_medico= %s WHERE cd_usuario = %s;'

SQL_SELECT_USUARIO_LOGIN = 'SELECT cd_usuario, cd_email, cd_senha, cd_medico FROM "Usuario" ' \
                           'WHERE cd_email= %s AND cd_senha = %s;'


class UsuarioDAO(Dao):
    def __init__(self, usuario=None):
        Dao.__init__(self)
        self.__usuario = usuario

    @property
    def usuario(self):
        return self.__usuario

    @usuario.setter
    def usuario(self, usuario):
        self.__usuario = usuario

    def salva(self):
        cursor = self.db.cursor()

        if self.usuario.cd_usuario:
            cursor.execute(SQL_UPDATE_USUARIO, (self.usuario.cd_email, self.usuario.cd_senha, self.usuario.cd_medico,
                                                self.usuario.cd_usuario))
        else:
            cursor.execute(SQL_INSERT_USUARIO, (self.usuario.cd_email, self.usuario.cd_senha, self.usuario.cd_medico))

            self.usuario.cd_usuario = cursor.fetchone()[0]

        self.db.commit()
        return self.usuario

    def login(self, cd_email, cd_senha):
        cursor = self.db.connection.cursor()
        cursor.execute(SQL_SELECT_USUARIO_LOGIN, (cd_email, cd_senha))
        usuario = cursor.fetchone()
        return Usuario(cd_usuario=usuario[0], cd_email=usuario[1], cd_senha=usuario[2], cd_medico=usuario[3])
