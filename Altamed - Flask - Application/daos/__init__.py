import psycopg2


db = psycopg2.connect(host='clinica-altamed-db.postgres.database.azure.com', database='altamed',
                      user='aps@clinica-altamed-db', password='Altadb@123#')


from .Dao import Dao
from .AcompanhamentoDAO import AcompanhamentoDAO
from .EnderecoDAO import EnderecoDAO
from .EspecialidadeDAO import EspecialidadeDAO
from .EstadiaDAO import EstadiaDAO
from .FormacaoDAO import FormacaoDAO
from .LogDAO import LogDAO
from .MedicoDAO import MedicoDAO
from .PacienteDAO import PacienteDAO
from .PessoaDAO import PessoaDAO
from .QuartoDAO import QuartoDAO
from .UsuarioDAO import UsuarioDAO
