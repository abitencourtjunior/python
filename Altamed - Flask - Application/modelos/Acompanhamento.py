class Acompanhamento(object):
    def __init__(self, cd_acompanhemento=None, dt_visita=None, nr_hora=None, de_estado=None, vl_pressao=None,
                 vl_temperatura=None, cd_medico_visita=None, cd_paciente=None):
        self.__cd_acompanhamento = cd_acompanhemento
        self.__dt_visita = dt_visita
        self.__nr_hora = nr_hora
        self.__de_estado = de_estado
        self.__vl_pressao = vl_pressao
        self.__vl_temperatura = vl_temperatura
        self.__cd_medico_visita = cd_medico_visita
        self.__cd_paciente = cd_paciente

    @property
    def cd_acompanhamento(self):
        return self.__cd_acompanhamento

    @cd_acompanhamento.setter
    def cd_acompanhamento(self, cd_acompanhamento):
        self.__cd_acompanhamento = cd_acompanhamento

    @property
    def dt_visita(self):
        return self.__dt_visita

    @dt_visita.setter
    def dt_visita(self, dt_visita):
        self.__dt_visita = dt_visita

    @property
    def nr_hora(self):
        return self.__nr_hora

    @nr_hora.setter
    def nr_hora(self, nr_hora):
        self.__nr_hora = nr_hora

    @property
    def de_estado(self):
        return self.__de_estado

    @de_estado.setter
    def de_estado(self, de_estado):
        self.__de_estado = de_estado

    @property
    def vl_pressao(self):
        return self.__vl_pressao

    @vl_pressao.setter
    def vl_pressao(self, vl_pressao):
        self.__vl_pressao = vl_pressao

    @property
    def vl_temperatura(self):
        return self.__vl_temperatura

    @vl_temperatura.setter
    def vl_temperatura(self, vl_temperatura):
        self.__vl_temperatura = vl_temperatura

    @property
    def cd_medico_visita(self):
        return self.__cd_medico_visita

    @cd_medico_visita.setter
    def cd_medico_visita(self, cd_medico_visita):
        self.__cd_medico_visita = cd_medico_visita

    @property
    def cd_paciente(self):
        return self.__cd_paciente

    @cd_paciente.setter
    def cd_paciente(self, cd_paciente):
        self.__cd_paciente = cd_paciente
