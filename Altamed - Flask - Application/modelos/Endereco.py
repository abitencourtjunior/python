class Endereco(object):
    def __init__(self, cd_endereco=None, nm_rua=None, nm_bairro=None, nm_cidade=None, nm_uf=None, cd_cep=None):
        self.__cd_endereco = cd_endereco
        self.__nm_rua = nm_rua
        self.__nm_bairro = nm_bairro
        self.__nm_cidade = nm_cidade
        self.__nm_uf = nm_uf
        self.__cd_cep = cd_cep

    @property
    def cd_endereco(self):
        return self.__cd_endereco

    @cd_endereco.setter
    def cd_endereco(self, cd_endereco):
        self.__cd_endereco = cd_endereco

    @property
    def nm_rua(self):
        return self.__nm_rua

    @property
    def nm_bairro(self):
        return self.__nm_bairro

    @property
    def nm_cidade(self):
        return self.__nm_cidade

    @property
    def nm_uf(self):
        return self.__nm_uf

    @property
    def cd_cep(self):
        return self.__cd_cep
