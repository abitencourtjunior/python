class Especialidade(object):
    def __init__(self, cd_especialidade=None, nm_especialidade=None):
        self.__cd_especialidade = cd_especialidade
        self.__nm_especialidade = nm_especialidade

    @property
    def cd_especialidade(self):
        return self.__cd_especialidade

    @property
    def nm_especialidade(self):
        return self.__nm_especialidade

    @cd_especialidade.setter
    def cd_especialidade(self, cd_especialidade):
        self.__cd_especialidade = cd_especialidade

    @nm_especialidade.setter
    def nm_especialidade(self, nm_especialidade):
        self.__nm_especialidade = nm_especialidade
