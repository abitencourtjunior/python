class Estadia(object):
    def __init__(self, cd_quarto, cd_paciente, fg_infeccao, dt_infeccao, de_sintomas):
        self.__cd_quarto = cd_quarto
        self.__cd_paciente = cd_paciente
        self.__fg_infeccao = fg_infeccao
        self.__dt_infeccao = dt_infeccao
        self.__de_sintomas = de_sintomas

    @property
    def cd_quarto(self):
        return self.__cd_quarto

    @property
    def cd_paciente(self):
        return self.__cd_paciente

    @property
    def fg_infeccao(self):
        return self.__fg_infeccao

    @property
    def dt_infeccao(self):
        return self.__dt_infeccao

    @property
    def de_sintomas(self):
        return self.__de_sintomas
