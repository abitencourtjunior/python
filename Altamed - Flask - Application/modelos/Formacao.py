class Formacao(object):
    def __init__(self, cd_medico=None, cd_especialidade=None, fg_ativo=None):
        self.__cd_medico = cd_medico
        self.__cd_especialidade = cd_especialidade
        self.__fg_ativo = fg_ativo

    @property
    def cd_medico(self):
        return self.__cd_medico

    @property
    def cd_especialidade(self):
        return self.__cd_especialidade

    @property
    def fg_ativo(self):
        return self.__fg_ativo

    @cd_medico.setter
    def cd_medico(self, cd_medico):
        self.__cd_medico = cd_medico

    @cd_especialidade.setter
    def cd_especialidade(self, cd_especialidade):
        self.__cd_especialidade = cd_especialidade

    @fg_ativo.setter
    def fg_ativo(self, fg_ativo):
        self.__fg_ativo = fg_ativo