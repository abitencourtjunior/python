from models.Utils import cria_data, cria_hora

class Log(object):
    def __init__(self, cd_log=None, de_log=None, dt_log=None, hr_log=None):
        self.__cd_log = cd_log
        self.__de_log = de_log
        self.__dt_log = cria_data(dt_log)
        self.__hr_log = cria_hora(hr_log)

    @property
    def cd_log(self):
        return self.__cd_log

    @property
    def de_log(self):
        return self.__de_log

    @property
    def dt_log(self):
        return self.__dt_log

    @property
    def hr_log(self):
        return self.__hr_log

    @cd_log.setter
    def cd_log(self, cd_log):
        self.__cd_log = cd_log

    @de_log.setter
    def de_log(self, de_log):
        self.__de_log = de_log

    @dt_log.setter
    def dt_log(self, dt_log):
        self.__dt_log = cria_data(dt_log)

    @hr_log.setter
    def hr_log(self, hr_log):
        self.__hr_log = cria_hora(hr_log)
