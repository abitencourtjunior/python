from models.Pessoa import Pessoa


class Medico(Pessoa):
    def __init__(self, pessoa, cd_medico=None, nr_crm=None, fg_ativo=None, vl_salario=None, especialidade=None):
        Pessoa.__init__(self, pessoa.nm_pessoa, pessoa.dt_nascimento, pessoa.nr_rg, pessoa.nr_cpf, pessoa.fg_genero,
                        pessoa.nr_telefone, cd_pessoa=pessoa.cd_pessoa, endereco_cd=pessoa.endereco_cd)

        self.__cd_medico = cd_medico
        self.__nr_crm = nr_crm
        self.__fg_ativo = fg_ativo
        self.__vl_salario = vl_salario
        self.__especialidade = especialidade

    @property
    def cd_medico(self):
        return self.__cd_medico

    @cd_medico.setter
    def cd_medico(self, cd_medico):
        self.__cd_medico = cd_medico

    @property
    def nr_crm(self):
        return self.__nr_crm

    @nr_crm.setter
    def nr_crm(self, nr_crm):
        self.__nr_crm = nr_crm

    @property
    def fg_ativo(self):
        return self.__fg_ativo

    @fg_ativo.setter
    def fg_ativo(self, fg_ativo):
        self.__fg_ativo = fg_ativo

    @property
    def vl_salario(self):
        return self.__vl_salario

    @vl_salario.setter
    def vl_salario(self, vl_salario):
        self.__vl_salario = vl_salario

    @property
    def especialidade(self):
        return self.__especialidade

    @especialidade.setter
    def especialidade(self, especialidade):
        self.__especialidade = especialidade
