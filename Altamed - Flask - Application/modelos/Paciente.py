from models.Pessoa import Pessoa
from models.Utils import cria_hora, cria_data


class Paciente(Pessoa):
    def __init__(self, pessoa, cd_paciente=None, de_diagnostico=None, cd_medico_responsavel=None,
                 dt_entrada_espera=None, hr_entrada_espera=None, fg_atendido=None, cd_gravidade=None):

        Pessoa.__init__(self, nm_pessoa=pessoa.nm_pessoa, dt_nascimento=pessoa.dt_nascimento, nr_rg=pessoa.nr_rg,
                        nr_cpf=pessoa.nr_cpf, fg_genero=pessoa.fg_genero, nr_telefone=pessoa.nr_telefone,
                        cd_pessoa=pessoa.cd_pessoa, endereco_cd=pessoa.endereco_cd)

        self.__cd_paciente = cd_paciente
        self.__de_diagnostico = de_diagnostico
        self.__cd_medico_responsavel = cd_medico_responsavel
        self.__dt_entrada_espera = cria_data(dt_entrada_espera)
        self.__hr_entrada_espera = cria_hora(hr_entrada_espera)
        self.__fg_atendido = fg_atendido
        self.__cd_gravidade = cd_gravidade

    @property
    def cd_paciente(self):
        return self.__cd_paciente

    @cd_paciente.setter
    def cd_paciente(self, cd_paciente):
        self.__cd_paciente = cd_paciente

    @property
    def de_diagnostico(self):
        return self.__de_diagnostico

    @de_diagnostico.setter
    def de_diagnostico(self, de_diagnostico):
        self.__de_diagnostico = de_diagnostico

    @property
    def cd_medico_responsavel(self):
        return self.__cd_medico_responsavel

    @cd_medico_responsavel.setter
    def cd_medico_responsavel(self, cd_medico_responsavel):
        self.__cd_medico_responsavel = cd_medico_responsavel

    @property
    def dt_entrada_espera(self):
        return self.__dt_entrada_espera

    @dt_entrada_espera.setter
    def dt_entrada_espera(self, dt_entrada_espera):
        self.__dt_entrada_espera = cria_data(dt_entrada_espera)

    @property
    def hr_entrada_espera(self):
        return self.__hr_entrada_espera

    @hr_entrada_espera.setter
    def hr_entrada_espera(self, hr_entrada_espera):
        self.__hr_entrada_espera = cria_hora(hr_entrada_espera)

    @property
    def fg_atendido(self):
        return self.__fg_atendido

    @fg_atendido.setter
    def fg_atendido(self, fg_atendido):
        self.__fg_atendido = fg_atendido

    @property
    def cd_gravidade(self):
        return self.__cd_gravidade

    @cd_gravidade.setter
    def cd_gravidade(self, cd_gravidade):
        self.__cd_gravidade = cd_gravidade
