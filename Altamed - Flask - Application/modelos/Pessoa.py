class Pessoa(object):
    def __init__(self, nm_pessoa=None, dt_nascimento=None, nr_rg=None, nr_cpf=None, fg_genero=None, nr_telefone=None,
                 cd_pessoa=None, endereco_cd=None):
        self.__cd_pessoa = cd_pessoa
        self.__nm_pessoa = nm_pessoa
        self.__dt_nascimento = dt_nascimento
        self.__nr_rg = nr_rg
        self.__nr_cpf = nr_cpf
        self.__fg_genero = fg_genero
        self.__nr_telefone = nr_telefone
        self.__endereco_cd = endereco_cd

    @property
    def cd_pessoa(self):
        return self.__cd_pessoa

    @cd_pessoa.setter
    def cd_pessoa(self, cd_pessoa):
        self.__cd_pessoa = cd_pessoa

    @property
    def nm_pessoa(self):
        return self.__nm_pessoa

    @nm_pessoa.setter
    def nm_pessoa(self, nm_pessoa):
        self.__nm_pessoa = nm_pessoa

    @property
    def dt_nascimento(self):
        return self.__dt_nascimento

    @dt_nascimento.setter
    def dt_nascimento(self, dt_nascimento):
        self.__dt_nascimento = dt_nascimento

    @property
    def nr_rg(self):
        return self.__nr_rg

    @nr_rg.setter
    def nr_rg(self, nr_rg):
        self.__nr_rg = nr_rg

    @property
    def nr_cpf(self):
        return self.__nr_cpf

    @nr_cpf.setter
    def nr_cpf(self, nr_cpf):
        self.__nr_cpf = nr_cpf

    @property
    def fg_genero(self):
        return self.__fg_genero

    @fg_genero.setter
    def fg_genero(self, fg_genero):
        self.__fg_genero = fg_genero

    @property
    def nr_telefone(self):
        return self.__nr_telefone

    @nr_telefone.setter
    def nr_telefone(self, nr_telefone):
        self.__nr_telefone = nr_telefone

    @property
    def endereco_cd(self):
        return self.__endereco_cd

    @endereco_cd.setter
    def endereco_cd(self, endereco_cd):
        self.__endereco_cd = endereco_cd
