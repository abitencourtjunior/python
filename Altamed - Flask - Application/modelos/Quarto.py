class Quarto(object):
    def __init__(self, cd_quarto=None, nr_andar=None, nr_capacidade=None, nr_quarto=None):
        self.__cd_quarto = cd_quarto
        self.__nr_andar = nr_andar
        self.__nr_capacidade = nr_capacidade
        self.__nr_quarto = nr_quarto

    @property
    def cd_quarto(self):
        return self.__cd_quarto

    @cd_quarto.setter
    def cd_quarto(self, cd_quarto):
        self.__cd_quarto = cd_quarto

    @property
    def nr_andar(self):
        return self.__nr_andar

    @nr_andar.setter
    def nr_andar(self, nr_andar):
        self.__nr_andar = nr_andar

    @property
    def nr_capacidade(self):
        return self.__nr_capacidade

    @nr_capacidade.setter
    def nr_capacidade(self, nr_capacidade):
        self.__nr_capacidade = nr_capacidade

    @property
    def nr_quarto(self):
        return self.__nr_quarto

    @nr_quarto.setter
    def nr_quarto(self, nr_quarto):
        self.__nr_quarto = nr_quarto
