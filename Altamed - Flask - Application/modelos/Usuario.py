class Usuario(object):
    def __init__(self, cd_usuario=None, cd_email=None, cd_senha=None, cd_medico=None):
        self.__cd_usuario = cd_usuario
        self.__cd_email = cd_email
        self.__cd_senha = cd_senha
        self.__cd_medico = cd_medico

    @property
    def cd_usuario(self):
        return self.__cd_usuario

    @cd_usuario.setter
    def cd_usuario(self, cd_usuario):
        self.__cd_usuario = cd_usuario

    @property
    def cd_email(self):
        return self.__cd_email

    @cd_email.setter
    def cd_email(self, cd_email):
        self.__cd_email = cd_email

    @property
    def cd_senha(self):
        return self.__cd_senha

    @cd_senha.setter
    def cd_senha(self, cd_senha):
        self.__cd_senha = cd_senha

    @property
    def cd_medico(self):
        return self.__cd_medico

    @cd_medico.setter
    def cd_medico(self, cd_medico):
        self.__cd_medico = cd_medico
