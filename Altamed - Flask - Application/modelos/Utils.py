from datetime import date, time


def cria_data(data):
    if data:
        return data
    else:
        return date.today()


def cria_hora(hora):
    if hora:
        return hora
    else:
        timeagora = time()
        return time.strftime(timeagora, '%H:%M:%S')