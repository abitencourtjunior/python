from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
import os

# Criando o chatbot
bot = ChatBot("Altabot")

# Definindo professor
bot.set_trainer(ListTrainer)

for sabedoria in os.listdir("base"):
    memoria = open('base/'+sabedoria, 'r').readlines()
    bot.train(memoria)

    pessoa = input("Digite o seu nome: ")
    
while True:

    entrada = input('{}: '.format(pessoa))

    bot_response = bot.get_response(entrada)
    bot_response = str(bot_response)
    print("Altabot: "+bot_response)
