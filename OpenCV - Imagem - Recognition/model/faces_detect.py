import cv2

# Carrega a imagem
image_path = '../img/002.jpeg'

# Carrega o modelo de reconhecimento facial
cascade_path = '../util/haarcascade_frontalface_default.xml'

# Criar o classificador de imagens
clf =  cv2.CascadeClassifier(cascade_path)

# Realizar a leitura de uma imagem
image = cv2.imread(image_path)

# Conversão de imagem em escala de cinza
image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

faces = clf.detectMultiScale(image_gray, 1.1, 10)

for (x, y, w, h) in faces:
    #Varre a imagem e desenha o retangulo
    image = cv2.rectangle(image, (x, y), (x+w, y+h), (255,255,0), 2)

cv2.imshow('Reconhecimento de Face - OpenCV',image)
cv2.waitKey(0)
cv2.destroyAllWindows()