import cv2

# Carrega a imagem
image_path = '../img/002.jpeg'

# Realizar a leitura de uma imagem
image = cv2.imread(image_path, 0)

cv2.imshow('Imagem em escala de Cinza', image)
cv2.waitKey(0)
cv2.destroyAllWindows()