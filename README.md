**Sobre o Repositório**

O intuito deste diretório é mostrar as funcionalidades que a linguagem em si tem e suas aplicações, tanto no ambito acadêmico quando de mercado. São envolvidas diversos paradigmas de desenvolvimento e também padrões de projetos conhecidos tais como MVC, POO e outros. Além destes há também alguns algoritmos que movem casos mais complexos e imprevisiveis como inteligência artifical, o qual para estes o intuito é mostrar a criação desde o cerne para ter todo o engajamento quando aplicada em qualquer situação. 

Tópicos: 

1. Paradigmas de Orientação a Objetos; 
2. Chatbots com Machine Learning
3. Desenvolvimento de Testes Unitários e TDD;
4. Aplicações Web utilizando Django e Flask
5. Conexão com Banco de Dados e aplicações com o mesmo; 
6. Estrutura de Dados ( Filas, Pilhas ) 
7. Tópicos de computação quántica ( Em Breve )
8. Inteligência Artificial ( Primeiros Tópicos ja desenvolvidos )
9. Melhorias em IA ( Em breve ) 
