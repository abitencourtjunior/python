import json

class cep (object):

    def __init__(self, rua, cidade, uf):
        self.__rua = rua
        self.__cidade = cidade
        self.__uf = uf

    def __str__(self):
        return f'{self.__uf} : {self.__cidade} : {self.__rua}'

    @property
    def rua(self):
        return self.__rua

    @property
    def cidade(self):
        return self.__cidade

    @property
    def uf(self):
        return self.__uf