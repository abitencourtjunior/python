from cep import cep
import json
import requests

# from pandas import DataFrame, ExcelFile
# import pandas as pd
#
# file = 'ceps.xlsx'
#
# xl = pd.read_excel(r'ceps.xlsx', sheet_name='Plan1')
#
# print(xl.all)

class ViaCep:

        def __init__(self, cep):
            self.cep = cep

        def getDadosCep(self):
            url_api = (f'http://www.viacep.com.br/ws/{cep.uf}/{cep.cidade}/{cep.rua}json')
            req = requests.get(url_api)
            if req.status_code == 200:
                dados_json = json.loads(req.text)
                return dados_json



teste = cep("Rua Jaco mariano","sao jose","sc")
viacepteste = ViaCep(teste)
viacepteste.getDadosCep()

