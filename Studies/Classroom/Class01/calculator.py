from Classroom.Class10.Exercicio001 import saudacao
class Calculadora():

    def __init__(self, n1, n2):
        self.__n1 = n1
        self.__n2 = n2

    @saudacao
    def soma(self):
        return self.__n1+self.__n2


    def subtracao(self):
        return self.__n1-self.__n2

    def multiplica(self):
        return self.__n1 * self.__n2

    def divisao(self):
        return self.__n1 / self.__n2


teste = Calculadora()
print(teste.soma)