class VerificarImpar:

    __numero = ''

    def __init__(self, numero):
        self.__numero = numero

    def verificar_valor_positivo(self):
        if self.__numero >= 0:
            return True
        else:
            return False

    def verifica_valor_par(self):
        if self.__numero % 2 == 0:
            return True
        else:
            return False

    def verificacao(self):
        if self.__numero>=0:
            if self.__numero % 2 == 0:
                return print('Valor positivo e par')
            else:
                return print('Valor positivo e impar')
        else:
            if(self.__numero % 2 == 0):
                return print('Valor negativo e par')
            else:
                return print('Valor negativo e impar')