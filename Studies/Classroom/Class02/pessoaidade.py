class pessoa:

    __nome: ''
    __idade: ''
    __sexo: ''
    __media: ''

    def __init__(self, nome, idade, sexo):
        self.__nome = nome
        self.__idade = idade
        self.__sexo = sexo

    def verifica_idade(self):
        if (self.__idade>=18):
            print("Você é maior de 18 anos, caro(a) "+self.__nome)
            return self.__idade
        else:
            print('Você é menor de idade! Caro(a) '+self.__nome)
            return self.__idade

    def idade(self):
        return self.__idade

    def calcula_media(self, primeira, segunda, terceira):
        self.__media = (primeira+segunda+terceira) / 3
        print("Sua média é: {}".format(self.__media))

    def aprovado(self, nome):
        if(self.__media >= 7):
            print("{}, você está APROVADO\n".format(nome))
        else:
            print("{}, você está REPROVADO\n".format(nome))

    def cadastra_alunos(self):
        for i in range(5):
            nome_aluno = str(input("Digite o nome do aluno: ")).strip().title()
            self.calcula_media(float(input("Digite uma nota: ")), float(input("Digite uma nota: ")),
                          float(input("Digite uma nota: ")))
            self.aprovado(nome_aluno)
