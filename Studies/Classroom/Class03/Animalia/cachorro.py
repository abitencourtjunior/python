from Classroom.Class03.Animalia.animal import animalModelo

class cachorro(animalModelo):

    def __init__(self,nome):
        super().__init__(nome)

    def comer(self):
        print("Cachorro - O {} está comendo sua ração.".format(self.nome))

