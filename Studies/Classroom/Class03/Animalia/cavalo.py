from Classroom.Class03.Animalia.animal import animalModelo

class cavalo(animalModelo):

    def __init__(self,nome):
        super().__init__(nome)

    def comer(self):
        print("Cavalo - O {} está comendo o seu capim.".format(self.nome))