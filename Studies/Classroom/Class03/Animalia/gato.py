from Classroom.Class03.Animalia.animal import animalModelo

class gato(animalModelo):

    def __init__(self,nome):
        super().__init__(nome)

    def comer(self):
        return print("Gato - O {} está comendo seu peixe.".format(self.nome))