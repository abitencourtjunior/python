# Exercício Original 20

from Classroom.Class03.QuadroFuncionarios.Funcionario import funcionario

class Analista(funcionario):

    def __init__(self, nome, idade, salario):
        super().__init__(nome, idade, salario)

    def aumenta_salario(self):
            self.salario = self.salario + 20
            return self.salario

