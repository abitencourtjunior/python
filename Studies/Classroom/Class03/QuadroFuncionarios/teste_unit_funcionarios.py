import unittest

from Classroom.Class03.QuadroFuncionarios.Analista import Analista
from Classroom.Class03.QuadroFuncionarios.Programador import programador

class teste_funcionario(unittest.TestCase):

    def teste_aumento_analista(self):
        analista = Analista("Afonso", "20", 1800)
        self.assertNotEqual(analista.aumenta_salario(20), analista.salario)

    def teste_aumento_programador(self):
        developer = programador("Altamir", "20", 1700)
        self.assertNotEqual(developer.aumenta_salario(30), developer.salario)