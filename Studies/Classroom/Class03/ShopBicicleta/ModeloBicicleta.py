# Exercicio Original - 19

class Bicicleta:

    def __init__(self, cor, freio, marca, qtd_marcha):
        self.__cor = cor
        self.__freio = freio
        self.__marca = marca
        self.__qtd_marcha = int(qtd_marcha)

    def quantidadeMarchas(self):
        print("Quantidade de Marchas: "+ str(self.__qtd_marcha))

    def tipoFreio(self):
        print("Tipo de Freio: " + str(self.__freio))

    def marca(self):
        print("Marca: " + str(self.__marca))

    def cor(self):
        print("Cor: " + str(self.__cor))

    def mostrar_informacoes(self):
        self.marca()
        self.cor()
        self.quantidadeMarchas()
        self.tipoFreio()

