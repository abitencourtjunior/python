class calculadora:

    __n1 = ''
    __n2 = ''

    def __init__(self,n1,n2):
        self.n1 = n1
        self.n2 = n2

    def soma(self):
        return self.n1+self.n2

    def subtracao(self):
        return self.n1-self.n2

    def multiplica(self):
        return self.n1 * self.n2

    def divisao(self):
        return self.n1 / self.n2

    def calcular_tudo(self):
        print("O resultado da soma: "+ str(self.soma()))
        print("O resultado da subtração: "+ str(self.subtracao()))
        print("O resultado da multiplicação: "+ str(self.multiplica()))
        print("O resultado da divisão: "+ str(self.divisao()))

teste = calculadora(10,20)
teste.calcular_tudo()



