class caixaregistradora:

    def __init__(self,dinheiro, valorproduto, desconto):
        self.__dinheiro = dinheiro
        self.__valorproduto = valorproduto
        self.__desconto = desconto

    def desconto_produto(self):
        self.__valorproduto -= self.__valorproduto * self.__desconto

    def calculatroco(self):

        self.desconto_produto()

        self.__troco = self.__dinheiro - self.__valorproduto

        if(self.__troco > 0):
            print('O seu troco é de R$ ' + str(self.__troco))
        elif(self.__troco == 0):
            print('Não há troco a ser retornado.')
        else:
            print('Você terá que inteirar o dinheiro, pois faltou R$ '+ str(self.__troco))


teste = caixaregistradora(100,10,0.10)
teste.calculatroco()