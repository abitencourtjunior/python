# Exercicio Original - 12

class maiorValor:

    def __init__(self, n1, n2):
        self.__n1 = n1
        self.__n2 = n2

    def verificar_maior(self):
        if(self.__n1>self.__n2):
            print("O valor {} é maior que {}".format(self.__n1,self.__n2))
        elif (self.__n2>self.__n1):
            print("O valor {} é maior que {}".format(self.__n2,self.__n1))
        else:
            print('Os valores são iguais!')

teste = maiorValor(int(input("Digite o primeiro valor: ")),int(input("Digite o segundo valor: ")))
teste.verificar_maior()