from Classroom.Class04.Festa.IngressoVip import Vip

class Camarote_Inferior(Vip):

    def __init__(self, valor, adicional, localizacao):
        Vip.__init__(self, valor, adicional)
        self.__localizacao = localizacao

    def mostrar_localizacao(self):
        return self.__localizacao
