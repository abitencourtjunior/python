from Classroom.Class04.Festa.IngressoVip import Vip

class Camarote_Superior(Vip):

    def __init__(self, valor, adicional, camarotevalor):
        Vip.__init__(self, valor, adicional)
        self.__camarotevalor = camarotevalor

    def comprar_ingresso_camarote(self):
        ingresso_camarote = self.comprar_ingresso_vip() + self.__camarotevalor
        return ingresso_camarote