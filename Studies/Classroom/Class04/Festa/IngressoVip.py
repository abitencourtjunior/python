from Classroom.Class04.Festa.Ingresso import Ingresso

class Vip(Ingresso):
    def __init__(self, valor, adicional):
        Ingresso.__init__(self, valor)
        self.__adicional = adicional

    def mostrar_ingresso_vip(self):
        return self.mostrar_valor()

    def comprar_ingresso_vip(self):
        ingresso_vip = self.mostrar_valor() + self.__adicional
        return ingresso_vip


teste = Vip(100,20)
print(teste.comprar_ingresso_vip())
