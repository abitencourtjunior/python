from Classroom.Class04.MinhaCasaMinhaVida.Imovel import Imovel

class Novo(Imovel):

    def __init__(self, endereco, preco, adicional):
        Imovel.__init__(self, endereco, preco)
        self.__adicional = adicional

    def valor_adicional(self):
        valor = self.preco_imovel() + self.__adicional
        return valor

