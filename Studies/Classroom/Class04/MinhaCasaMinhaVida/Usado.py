from Classroom.Class04.MinhaCasaMinhaVida.Imovel import Imovel

class Usado(Imovel):

    def __init__(self, endereco, preco, desconto):
        Imovel.__init__(self, endereco, preco)
        self.__desconto = desconto

    def aplicar_desconto(self):
        desconto_usado = self.preco_imovel() -((self.preco_imovel() * (self.__desconto/100)))
        return desconto_usado

