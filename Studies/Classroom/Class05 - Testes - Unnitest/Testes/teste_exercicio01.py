# Exercício Calculadora

import unittest
from Classroom.Class01.calculator import Calculadora

class testeunitario(unittest.TestCase):

    calculadora = Calculadora(1, 1)

    def teste_calculadora(self):
        self.assertEqual(self.calculadora.soma(), 2)
        self.assertNotEqual(self.calculadora.soma(), 5)

    def teste_calculadora_multiplicacao(self):
        self.assertEqual(self.calculadora.multiplica(), 1)

