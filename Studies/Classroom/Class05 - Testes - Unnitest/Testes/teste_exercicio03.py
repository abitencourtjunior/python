import unittest

from Classroom.Class02.vendaloja import caixaregistradora

class teste_unitario(unittest.TestCase):

    caixa_teste = caixaregistradora(100, 50)

    def teste_troco_caixa_registradora(self):
        self.assertEqual(self.caixa_teste.calculatroco(), 50)

    def teste_diferenca_caixa_registradora(self):
        self.assertNotEqual(self.caixa_teste.calculatroco(), 40)
