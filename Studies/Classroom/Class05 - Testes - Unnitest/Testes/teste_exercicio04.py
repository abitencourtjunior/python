import unittest

from Classroom.Class02.VerificarImpar import VerificarImpar

class TesteUnitario(unittest.TestCase):

    teste = VerificarImpar(4)

    def teste_numero_positivo(self):
        self.assertEqual(self.teste.verificar_valor_positivo(), True)
        self.assertTrue(self.teste.verificar_valor_positivo())