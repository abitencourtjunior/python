import unittest

from Classroom.Class03.exercicio04 import teclado

class TesteUnitario(unittest.TestCase):

    valor = teclado(10)

    def test_verificar_antecessor(self):
        self.assertEqual(self.valor.antecessor(), 9)
