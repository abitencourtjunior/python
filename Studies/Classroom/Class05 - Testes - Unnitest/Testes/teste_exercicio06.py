import unittest

from Classroom.Class03.exercicio08 import volumeretangulo

class TesteVolumeRetangulo(unittest.TestCase):

    vol_retangulo = volumeretangulo(1,1,1)

    def teste_volume_retangulo(self):
        self.assertEqual(self.vol_retangulo.calcular_volume(), 1)
