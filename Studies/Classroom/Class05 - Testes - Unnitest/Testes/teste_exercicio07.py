import unittest

from Classroom.Class03.ShopBicicleta.BicicletaProfessional import profissional

class TesteUnitarioBicicleta(unittest.TestCase):

     bike_pro = profissional("Azul", "Disco", "Caloi", 21)

     def teste_verificar_marchas(self):
         self.assertEqual(self.bike_pro.quantidadeMarchas(), 21)