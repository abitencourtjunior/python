# Exercício - Class 04 - Festa - Ingresso VIP

import unittest
from Classroom.Class04.Festa.IngressoVip import Vip

class testeingresso(unittest.TestCase):

    ingresso = Vip(100, 20)

    def teste_ingresso_vip(self):
        self.assertEqual(self.ingresso.comprar_ingresso_vip(), 120)

    def teste_mostrar_valor_ingresso(self):
        self.assertTrue(self.ingresso.mostrar_valor(), 120)