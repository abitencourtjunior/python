# Exercício Minha Casa Minha Vida - Classes Testes - Casa nova, Casa Usada

import unittest

from Classroom.Class04.MinhaCasaMinhaVida.Novo import Novo
from Classroom.Class04.MinhaCasaMinhaVida.Usado import Usado


class testeminhacasaminhavida(unittest.TestCase):

    testenovo = Novo("Rua A", 50000, 10000)
    testeusado = Usado("Rua B", 35000, 20)

    def teste_casa_nova(self):
        self.assertEqual(self.testenovo.valor_adicional(), 60000)
        self.assertNotEqual(self.testenovo.preco_imovel(), 60000)

    def teste_casa_usada(self):
        self.assertEqual(self.testeusado.aplicar_desconto(), 28000)