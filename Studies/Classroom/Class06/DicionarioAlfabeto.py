class Alfabeto:

    def __init__(self):
        self.__molde = {"0": " ", "1": 'a', '2': 'b', '3': 'c', '4': 'd', '5': 'e', '6': 'f', '7': 'g', '8': 'h', '9': 'i','10': 'j',
                 '11': 'k', '12': 'l', '13': 'm', '14': 'n', '15': 'o', '16': 'p', '17': 'q', '18': 'r', '19': 's', '20':'t',
                 '21':'u', '22': 'v', '23': 'w', '24': 'x', '25': 'y', '26': 'z'}
        self.__lista = []
        self.__cifra = []
        self.__traducao = []

    def transformar(self, palavra):
        for i in range(len(palavra)):
            self.__lista.append(palavra[i])

    def encriptar(self):
        for contador in range(len(self.__lista)):
            for key, value in self.__molde.items():
                if value == self.__lista[contador]:
                    self.__cifra.append(key)

    def traduzir(self):
        for contador in range(len(self.__lista)):
            for key, value in self.__molde.items():
                if key == self.__cifra[contador]:
                    self.__traducao.append(value)

    def mostrarEncriptado(self):
        return self.__cifra

    def mostrarTraduzido(self):
        return self.__traducao



teste = Alfabeto()
teste.transformar(input("Digite uma frase: ").lower())
teste.encriptar()
teste.traduzir()
teste.mostrarEncriptado()
teste.mostrarTraduzido()


