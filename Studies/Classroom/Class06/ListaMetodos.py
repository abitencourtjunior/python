# 23 - Crie uma programa que recebe uma lista qualquer e:
# a. retorne o maior elemento
# b. retorne a soma dos elementos
# c. retorne o número de ocorrências do primeiro elemento da lista
# d. retorne a média dos elementos

class Lista():

    def __init__(self):
        self.__lista = []

    def inserir_dados(self, iteracoes):
        for i in range(iteracoes):
            self.__lista.append(int(input("Digite o {} valor: ".format(i))))

    def maior_elemento(self):
        return max(self.__lista)

    def menor_elemento(self):
        return min(self.__lista)

    def ocorrencias_valores(self, valor):
        return self.__lista.count(valor)

    def media_lista(self):
        return sum(self.__lista) / len(self.__lista)




