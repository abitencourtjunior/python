# 26 - Faça um programa que receba duas listas e retorne
# True se são iguais ou False caso contrário,
# além do número de ocorrências do primeiro elemento da lista.

class ListaComparada:
    verificar = 0

    def __init__(self):
        lista_A = []
        lista_B = []

        self.__listaA = lista_A
        self.__listaB = lista_B

    def popularListas(self, numero):
        for i in range (int(numero)):
           self.__listaA.append(input("Digite o valor para a 1ª Lista: "))
           self.__listaB.append(input("Digite o valor para a 2ª Lista: "))

    def mostrarDados(self):
        for valorA in self.__listaA:
            print(self.__listaA[valorA])
            print(self.__listaB[valorA])

    def compararListas(self):
        for i in range(len(self.__listaA)):
            if(self.__listaA.__getitem__(i) == self.__listaB.__getitem__(i)):
                self.verificar += 1
                if(self.verificar == len(self.__listaA)):
                    return True
        return False

    def verificarOcorrencia(self):
        return self.__listaA


a = ListaComparada()
a.popularListas(3)
print(a.compararListas())
