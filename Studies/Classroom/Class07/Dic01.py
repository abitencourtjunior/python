class NomeDic:
    def __init__(self):
        self.__dictNome = {}
        self.popularNumero = 5
        self.__dictDuplicado = {}

    def popular(self):
        for contador in range(self.popularNumero):
            self.__dictNome[input("Digite a sua idade: ")] = input("Digite o seu nome: ")

    def mostrarDict(self):
        print(self.__dictNome)

    def duplicarDict(self):
        self.__dictDuplicado = self.__dictNome.copy()
        return print(self.__dictDuplicado)

teste = NomeDic()
teste.popular()
teste.mostrarDict()
teste.duplicarDict()
