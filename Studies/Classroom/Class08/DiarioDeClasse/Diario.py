from Classroom.Class03.exercicio01 import verificar
from Classroom.Class08.ReadFiles.ReadFile import leitura

class Diario:

    def __init__(self):
        self.__somarNotas = []
        self.__temp = 0
        pass

    def lerArquivo(self, nomeDoArquivo):
        self.__leitura = open(nomeDoArquivo+".txt", 'r')
        return self.__leitura

    def gravarArquivo(self, nomeDoArquivo, conteudo):
        self.__escrever = open(nomeDoArquivo+".txt", 'a')
        self.__escrever.write(conteudo)
        self.__escrever.close()
        return True

    def filtrarPalavras(self, nomeDoArquivo, comparar):
        for linha in self.lerArquivo(nomeDoArquivo):
            if linha.__contains__(comparar):
                return linha

    def gravarNotas(self):
        for alunos in range(1, 3):
            self.__notas = verificar()
            self.gravarArquivo("Nome", self.__notas.cadastrarAluno() + "\n ")
            for contador in range(1, 3):
                self.gravarArquivo("Notas",str(+self.__notas.cadastrarNota()) +",")

    def gravarMedia(self):
        self.notasGravadas()
        for contador in self.lerArquivo("Nome"):
            self.gravarArquivo("DiarioDeClasse", str(contador)+": "+str(self.__temp/3))


teste = Diario()
teste.gravarNotas()
teste.gravarMedia()