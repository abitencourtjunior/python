class leitura:

    __caminho = "Notas.txt"

    def __init__(self):
        self.__arquivo = open(self.__caminho, 'r')
        self.__arquivo_escrever = open(self.__caminho, 'a')

    def mostrarDados(self):
        for linha in self.__arquivo:
            print(linha)
        self.__arquivo.close()

    def filtrar(self, conteudo):
        for linha in self.__arquivo:
            if linha.__contains__(conteudo):
                return True

    def escrever(self, conteudo):
        self.__arquivo_escrever.write(conteudo)
        self.__arquivo_escrever.close()

