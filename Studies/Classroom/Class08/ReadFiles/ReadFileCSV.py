class leituraCSV:

    def __init__(self):
        self.__arquivo = "LeituraCSV.txt"
        self.__ler = open(self.__arquivo, 'r')

    def mostrarConteudo(self):
        for linha in self.__ler:
            valores = linha.split(",")
            print("Aluno: ", valores[0], "Obteve nota:", valores[1])


teste = leituraCSV()
teste.mostrarConteudo()