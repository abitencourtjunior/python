# Padrão que compartilha a variavel

def calculadora(valor):

    def soma(valor2):
        return valor + valor2 # - Isto é uma Nested Function
    return soma # - Função Closure

# Neste ponto estou passando o valor da função calculadora
# para poder executar a segunda funcao!

soma2 = calculadora(10)
soma3 = calculadora(10)

print(soma2(2))
print(soma3(2))