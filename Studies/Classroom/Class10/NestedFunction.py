# Decorator - Método de Interface

# Pesquisar padrões de projetos - Desenvolvimento


# Nested Function - Também conhecidas como funções aninhadas em Python. Ela serve para poder executar métodos
# pelo coneito de interface no Java

def party():
    print("Estou de fora!")

    def start_party():
        return "Estou dentro!"

    def finish_party():
        return "A festa acabou!"
    print(start_party())
    print(finish_party())

#Teste
party()