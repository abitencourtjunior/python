import unittest
# from Classroom.Class10.NestedFunction import party
from Classroom.Class10.Closure import calculadora


class teste_de_decorator_closure(unittest.TestCase):
    # def teste_start(self):
    #   self.assertEqual(party(), "Estou de fora!")

    soma = calculadora(10)

    def teste_soma(self):
        self.assertEqual(self.soma(10), 20)
