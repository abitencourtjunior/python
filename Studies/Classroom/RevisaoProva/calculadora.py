class calculadora:

    def __init__(self, valorA, valorB):
        self.__valorA = valorA
        self.__valorB = valorB

    def somar(self):
        return self.__valorA + self.__valorB

    def subtracao(self):
        return self.__valorA - self.__valorB

    def multiplicacao(self):
        return self.__valorA * self.__valorB

    def divisao(self):
        if self.__valorA == 0 or self.__valorB == 0:
            return print("Não é possível realizar divisão por ZERO !!")
        else:
            return self.__valorA / self.__valorB
