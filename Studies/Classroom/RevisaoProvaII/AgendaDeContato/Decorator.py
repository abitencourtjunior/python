# Exercicio de Decorator

# Definir a função que deseja para o decorator
#
#
# def teste_decorator(funcao):
#     # def fala():
#     #     print("Teste")
#     #     #Função como parametro, quando chamar colocando a @ nome do decorator
#     #     funcao()
#     #     print("Função")
#     # return fala


#     def calcular():
#         print("Cálculo")
#         valor = funcao()
#         return valor + 2
#         print(teste)
#     return calcular
def criar_log():
    log = open("Log.txt",'a')
    log.write("Teste de Log")
    log.close()


def escrever(conteudo):
    log = open('Log.txt', 'a')
    def gerar_log():
        log.write("Contato Inserido com Sucesso!")
        conteudo()
        log.close()
    return gerar_log



@escrever
def ola():
    return "Teste"

print(ola)
