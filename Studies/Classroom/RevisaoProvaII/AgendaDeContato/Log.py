def pass_thru(func_to_decorate):
    def new_func(*original_args, **original_kwargs):
        print ("Function has been decorated.  Congratulations.")
        log = open("Log.txt", 'a')
        log.write("Teste de Log"+str(original_args))
        log.close()
        # Do whatever else you want here
        return func_to_decorate(*original_args, **original_kwargs)
    return new_func

