

# Nested Fuction;

# def soma(valor):
#
#     def soma2(num):
#         return valor + num
#     return soma2
#
# soma2 = soma(5)
# print(soma2(5))


# Outro Exemplo - Multiplicação

def primeiro(valor):

    def multiplicacao(valor2):
        return valor * valor2
    return multiplicacao

multiplicacao = primeiro(20)
print("Aplicando o valor na váriavel")
print(multiplicacao(18))


def festa():
    print("Começando")

    def anda_festa():
        return "Andamento"

    def termino():
        return "Acabou"

    print(anda_festa())
    print(termino())


print(festa())


