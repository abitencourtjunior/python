class bytesCalculo:

    def __init__(self, capacidade):
        self._capacidade = capacidade

    def fabricante(self):
        return 1000 * 1000 * 1000

    def binario(self):
        return 1024 * 1024 * 1024

    def mostrarCapacidade(self):
        return self._capacidade

    def calculaBytesBinario(self):
        return self._capacidade * bytesCalculo.binario(self)

    def calcularBytesFabricante(self):
        return self._capacidade * bytesCalculo.fabricante(self)

    def diferencaCapacidade(self):
        return bytesCalculo.binario(self) - bytesCalculo.fabricante(self)

    def calcularCapacidadeReal(self):
        return round((self._capacidade - (bytesCalculo.diferencaCapacidade(self) * self._capacidade) / bytesCalculo.binario(self)),3)

teste = bytesCalculo(40)
print("A capacidade comercial de {} GB tem o valor real de armazenamento de {} GB".format(teste.mostrarCapacidade(),teste.calcularCapacidadeReal()))