class fila:

    def iniciar_fila(self):
        self.valores = []
        self.primeiro = -1
        self.ultimo = 0
        self.total = 0

    def inserir(self, valor):
        self.valores.insert(self.ultimo, valor)
        self.ultimo = (self.ultimo + 1 ) % len(self.valores)
        self.total+=1

    def retirar(self):
        temp = self.valores[self.primeiro]
        self.primeiro = (self.primeiro + 1) % len(self.valores)
        self.total-=1
        return temp

    def vazia(self):
        return self.total == 0

    def cheia(self):
        return self.total == len(self.valores)

    def exibe_fila(self):
        return self.valores

