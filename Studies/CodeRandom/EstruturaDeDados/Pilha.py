# Exercício 9

pilha = []
tamanho = 0;
topo_pilha = 0;
pilha_cheia =0;

def empilhar(valor):
    for contador in range(valor):
        pilha.append(int(input("Digite o valor do elemento:")))
    menu()

def desempilha():
    pilha.pop(len(pilha)-1)
    menu()

def mostrar_pilha():
    print(pilha)
    menu()

def pilha_vazia():
    if len(pilha) == 0:
        return print("Pilha Vazia!")
    else:
        pilha_cheia()
    menu()

def pilha_cheia():
    if tamanho == len(pilha):
        return print("Pilha está cheia!")
    else:
        print("A pilha ainda tem espaço: " + str(len(pilha) - tamanho))
    menu()

def menu():
    print("Pilha em Python")
    print("1 - Empilar")
    print("2 - Desempilar")
    print("3 - Mostrar Pilha")
    print("4 - Pilha Vazia")
    print("5 - Pilha Cheia")
    opcao = int(input("Digite a opção desejada: "))

    if opcao == 1:
        tamanho = int(input("Qual é o tamanho da pilha: "))
        empilhar(tamanho)
    elif opcao == 2:
        desempilha()
    elif opcao == 3:
        mostrar_pilha()
    elif opcao == 4:
        pilha_vazia()
    elif opcao == 5:
        pilha_cheia()
menu()