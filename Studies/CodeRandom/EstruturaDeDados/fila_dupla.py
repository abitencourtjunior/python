# Exercício 10

class fila_dupla:

    def iniciar_fila(self):
        self.valores = []
        self.primeiro = -1
        self.ultimo = 0
        self.total = 0

    def inserir_inicio(self, valor):
        self.valores.insert(self.ultimo, valor)
        self.ultimo = (self.ultimo + 1 ) % len(self.valores)
        self.total+=1

    def retirar_inicio(self):
        temp = self.valores[self.primeiro]
        self.primeiro = (self.primeiro + 1) % len(self.valores)
        self.total-=1
        return temp

    def inserir_final(self, valor):
        self.ultimo = (self.ultimo - 1 ) % len(self.valores)
        self.valores.insert(self.ultimo, valor)
        self.total+=1

    def retirar_final(self):
        temp = self.valores[self.ultimo]
        self.primeiro = (self.primeiro - 1) % len(self.valores)
        self.total-=1
        return temp


    def vazia(self):
        return self.total == 0

    def cheia(self):
        return self.total == len(self.valores)

teste = fila()
teste.iniciar_fila()
teste.inserir(1)
teste.inserir(2)
teste.inserir(3)
teste.inserir(4)
teste.retirar()
teste.retirar()
teste.retirar()
teste.retirar()


