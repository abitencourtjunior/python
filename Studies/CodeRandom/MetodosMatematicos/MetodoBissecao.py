# Método Matematico

import math

class Bissecao:

    def __init__(self, inicio, fim, tolerancia):
        self.__inicio = inicio
        self.__fim = fim
        self.__tolerancia = tolerancia

    def numero_de_iteracoes(self):
            resultado = (math.log((self.__fim - self.__inicio), 10) - math.log(self.__tolerancia, 10)
                         / math.log(2, 10)) - 1
            return resultado

    def arredondar(self):
        return float('%f' % (self.numero_de_iteracoes()))

