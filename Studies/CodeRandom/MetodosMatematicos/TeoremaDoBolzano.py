# Exercício do Bolsano

import math

class Bolsano:

     def __init__(self, n1,n2):
         self.__n1 = n1
         self.__n2 = n2

     def intervalo_inicial(self):
         ponto = math.pow(self.__n1, 3) - self.__n1 - 1
         return ponto

     def intervalo_final(self):
         ponto = math.pow(self.__n2, 3) - self.__n2 - 1
         return ponto

     def calculo_final(self):
         intervalo = self.intervalo_inicial() * self.intervalo_final()
         if(intervalo<0):
             print('Está dentro do Intervalo')
         else:
             print('Está fora do intervalo')