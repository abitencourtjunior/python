
from datetime import datetime
from progpy001 import listadecompras


class Pessoa():

    __datanascimento = ''
    __sexo = ''
    __nome = ''
    __lista = listadecompras


    def __init__(self, nome, datanascimento, sexo):
        self.nome = nome
        self.datanascimento = datanascimento
        self.sexo = sexo

    def idade(self):
        idade = datetime.now().year - self.datanascimento
        return idade

    def sexo_define(self):
        if self.sexo == 'M':
            return 'Masculino'
        elif self.sexo == 'F':
            return 'Feminino'
        else:
            return 'Indefinido'

    def nome(self):
        return self.nome()

    def adicionarlista(self, __lista):
        self.__lista.listaAdicionar()



