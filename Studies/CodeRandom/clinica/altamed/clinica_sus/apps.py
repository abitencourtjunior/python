from django.apps import AppConfig


class ClinicaSusConfig(AppConfig):
    name = 'clinica_sus'
