function cadastrar_paciente() {

    verificar_dados("atendido");
    verificar_dados("medico");
    verificar_dados("gravidade");

    let de_paciente;

    de_paciente = {
        cd_pessoa: document.getElementById("pessoa").value,
        de_diagnostico: document.getElementById("diagnostico").value,
        cd_medico_responsavel: document.getElementById("medico").value,
        dt_entrada_espera: document.getElementById("data_entrada").value,
        hr_entrada_espera: document.getElementById("hora_entrada").value,
        fg_atendido: document.getElementById("atendido").value
    }

    console.log(de_paciente)
}