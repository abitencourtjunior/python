function cadastrar_pessoa() {

    pessoa = {

        nm_pessoa: document.getElementById("nome").value,
        dt_nascimento: document.getElementById("datanascimento").value,
        nr_rg: document.getElementById("rg").value,
        nr_cpf: document.getElementById("cpf").value,
        fg_genero: document.getElementById("genero").value,
        nr_telefone: document.getElementById("telefone").value,

        endereco_cd: {
            nm_rua: document.getElementById("rua").value,
            nm_bairro: document.getElementById("bairro").value,
            nm_cidade: document.getElementById("cidade").value,
            nm_uf: document.getElementById("uf").value,
            cd_cep: document.getElementById("cep").value
        }
    };
    console.log(pessoa);
};