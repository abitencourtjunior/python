from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

class Connection:

    def session():
        connection_string = 'mysql+pymysql://%s:%s@%s:%s/%s' % (
            "root",
            "root",
            "localhost",
            "3306",
            "congregacao"
        )
        engine = create_engine(connection_string, echo=True)
        Session = sessionmaker(bind=engine)

        return Session()