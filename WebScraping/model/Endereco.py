class Endereco(object):

    def __init__(self, cidade, uf, pais, rua, numero, cep, localizacao):
        self.__cidade = cidade
        self.__pais = pais
        self.__rua = rua
        self.__cep = cep
        self.__uf = uf
        self.__numero = numero
        self.__localizacao = localizacao

    @property
    def cidade(self):
        return self.__cidade

    @property
    def pais(self):
        return self.__pais

    @property
    def rua(self):
        return self.__rua

    @property
    def numero(self):
        return self.__numero

    @property
    def cep(self):
        return self.__cep

    @property
    def uf(self):
        return self.__uf

    @property
    def localizacao(self):
        return self.__localizacao

    def __str__(self):
        return f'\nEndereço: {self.rua}, {self.numero}, {self.cidade}, {self.uf}, {self.pais} - {self.cep}' \
            f' : Google Maps: {self.localizacao}'