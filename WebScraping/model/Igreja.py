from model.Endereco import Endereco

class Igreja(object):

    def __init__(self, nome, cultos, rjm, localizacao, rua, numero, pais, cidade, uf, cep):
        self.__nome = nome
        self.__cultos = cultos
        self.__rjm = rjm
        self.__Endereco = Endereco(cidade, uf, pais, rua, numero, cep, localizacao)

    @property
    def nome(self):
        return self.__nome

    @property
    def cultos(self):
        return self.__cultos

    @property
    def rjm(self):
        return self.__rjm

    @property
    def endereco(self):
        return self.__Endereco

    def __str__(self):
        return f'{self.nome}, {self.cultos}, {self.rjm}, {self.endereco}'





